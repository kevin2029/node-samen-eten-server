const express = require('express');
const bodyParser = require('body-parser');
const infoRoutes = require('./src/routes/info.routes');
const studenthomeRoutes = require('./src/routes/studenthome.routes');
const mealRoutes = require('./src/routes/meal.routes');
const authenticationRoutes = require('./src/routes/authentication.routes');
const participantRoutes = require('./src/routes/participant.routes');
const config = require('./src/config/config');

const logger = config.logger;

const app = express();

app.use(bodyParser.json());

const port = process.env.PORT || 3000;

app.all('*', (req, res, next) => {
    const method = req.method;
    logger.info('Method: ', method);
    next();
});
app.use('/api', authenticationRoutes);
app.use('/api', infoRoutes);
app.use('/api', studenthomeRoutes);
app.use('/api', mealRoutes);
app.use('/api', participantRoutes);

app.all('*', (req, res, next) => {
    res.status(404).json({
        error: 'Endpoint does not exist'
    });
});

app.listen(port, () => logger.info(`Example app listening at port:${port}`));

module.exports = app;
