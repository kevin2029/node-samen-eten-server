process.env.DB_DATABASE = 'test_studenthome';
process.env.NODE_ENV = 'testing';

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');
const pool = require('../../src/config/database');
const jwt = require('jsonwebtoken');

chai.should();
chai.use(chaiHttp);

const CLEAR_DB = 'DELETE IGNORE FROM `participants`';
const INSERT_USER = `INSERT INTO user (First_Name, Last_Name, Email, Student_Number, Password) VALUES ('first', 'last', 'name2@server.nl','1234567', 'secret');`;

before((done) => {
    pool.query(CLEAR_DB, (err, rows, fields) => {
        if (err) {
            console.log(`beforeEach CLEAR error: ${err}`);
            done(err);
        } else {
            pool.query(INSERT_USER, (err, rows, fields) => {
                if (err) {
                    done(err);
                } else {
                    insertId = rows.insertId;
                    done();
                }
            });
        }
    });
});

after((done) => {
    pool.query(CLEAR_DB, (err, rows, fields) => {
        if (err) {
            console.log(`after error: ${err}`);
            done(err);
        } else {
            done();
        }
    });
});

describe('Aanmelden', () => {
    describe('UC-401 Aanmelden voor maaltijd', () => {
        it('TC-401-1 Niet ingelogd', function (done) {
            chai.request(server)
                .post('/studenthome/1/meal/1/signup')

                .send({
                    UserId: 5
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-401-2 Maaltijd bestaat niet', function (done) {
            chai.request(server)
                .post('/studenthome/1/meal/212132123/signup')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                )
                .send({
                    UserId: 5
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-401-3 Succesvol aangemeld', function (done) {
            chai.request(server)
                .post('/studenthome/1/meal/1/signup')
                .set('authorization', 'Bearer ' + jwt.sign({ id: 5 }, 'secret'))
                .send({
                    UserId: 5
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');
                    done();
                });
        });
    });
    describe('UC-402 Afmelden voor maaltijd', () => {
        it('TC-401-1 Niet ingelogd', function (done) {
            chai.request(server)
                .post('/studenthome/1/meal/1/signoff')
                .send({
                    UserId: 5
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-401-2 Maaltijd bestaat niet', function (done) {
            chai.request(server)
                .post('/studenthome/1/meal/212132123/signoff')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                )
                .send({
                    UserId: 5
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-402-3 Aanmelding bestaat niet', function (done) {
            chai.request(server)
                .post('/studenthome/132131313123/meal/1/signoff')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                )
                .send({
                    UserId: 5
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-401-3 Succesvol afgemeld', function (done) {
            chai.request(server)
                .post('/studenthome/1/meal/1/singoff')
                .set('authorization', 'Bearer ' + jwt.sign({ id: 5 }, 'secret'))
                .send({
                    UserId: 5
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');
                    done();
                });
        });
    });
    describe('UC-403 Lijst van deelnemers opvragen', () => {
        it('TC-403-1 Niet ingelogd', function (done) {
            chai.request(server)
                .get('/meal/1/participants')
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-403-2 Maaltijd bestaat niet', function (done) {
            chai.request(server)
                .get('/meal/54564/participants')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                )
                .send({
                    UserId: insertId
                })
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-403-3 Lijst van deelnemers geretourneerd', function (done) {
            chai.request(server)
                .get('/meal/1/participants')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                )
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');
                    done();
                });
        });
    });
    describe('UC-404 Details van deelnemer opvragen', () => {
        it('TC-404-1 Niet ingelogd', function (done) {
            chai.request(server)
                .get('/meal/1/participants')
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-404-2 Deelnemer bestaat niet', function (done) {
            chai.request(server)
                .get('/meal/1/participants/64564')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                )
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-404-3 Contactgegevens van deelnemer geretourneerd', function (done) {
            chai.request(server)
                .get('/meal/1/participants/1')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                )
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');
                    done();
                });
        });
    });
});
