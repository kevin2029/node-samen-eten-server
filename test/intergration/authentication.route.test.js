process.env.DB_DATABASE = 'test_studenthome';
process.env.NODE_ENV = 'testing';
process.env.LOGLEVEL = 'error';
console.log(`Running tests using database '${process.env.DB_DATABASE}'`);

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');
const pool = require('../../src/config/database');

chai.should();
chai.use(chaiHttp);

const CLEAR_DB = 'DELETE IGNORE FROM `user`';

before((done) => {
    pool.query(CLEAR_DB, (err, rows, fields) => {
        if (err) {
            console.log(`beforeEach CLEAR error: ${err}`);
            done(err);
        } else {
            done();
        }
    });
});

after((done) => {
    pool.query(CLEAR_DB, (err, rows, fields) => {
        if (err) {
            console.log(`after error: ${err}`);
            done(err);
        } else {
            done();
        }
    });
});

describe('Authenticatie', () => {
    describe('UC-101 Registreren', () => {
        it('TC-101-1 Verplicht veld ontbreekt', function (done) {
            chai.request(server)
                .post('/api/register')
                .send({
                    firstname: 'test',
                    // lastname: 'Nguyen',
                    email: 'row@test.com',
                    studentnr: '2150956',
                    password: 'secret5'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.an('object');

                    done();
                });
        });
        it('TC-101-2 Invalide email adres', function (done) {
            chai.request(server)
                .post('/api/register')
                .send({
                    firstname: 'test',
                    lastname: 'Nguyen',
                    email: 'rowtest.com',
                    studentnr: '2150956',
                    password: 'secret5'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-101-3 Invalide wachtwoord', function (done) {
            chai.request(server)
                .post('/api/register')
                .send({
                    firstname: 'test',
                    lastname: 'Nguyen',
                    email: 'test@test.com',
                    studentnr: '2150956',
                    password: 'secret'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-101-5 Gebruiker succesvol geregistreerd', function (done) {
            chai.request(server)
                .post('/api/register')
                .send({
                    firstname: 'Jan',
                    lastname: 'Smit',
                    email: 'test2@test.com',
                    studentnr: '222222',
                    password: 'secret5'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.an('object');

                    const response = res.body;
                    response.should.have.property('token').which.is.a('string');
                    response.should.have
                        .property('username')
                        .which.is.a('string');
                    done();
                });
        });
        it('TC-101-4 Gebruiker bestaat al', function (done) {
            chai.request(server)
                .post('/api/register')
                .send({
                    firstname: 'Jan',
                    lastname: 'Smit',
                    email: 'test2@test.com',
                    studentnr: '222222',
                    password: 'secret5'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.an('object');
                    done();
                });
        });
    });

    describe('UC-102 Login', () => {
        it('TC-102-1 Verplicht veld ontbreekt', (done) => {
            chai.request(server)
                .post('/api/login')
                .send({
                    // email: 'bcrypt4@test.com',
                    password: 'secret234'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-102-2 Invalide email adres', function (done) {
            chai.request(server)
                .post('/api/login')
                .send({
                    email: 'bcrypt4test.com',
                    password: 'secret234'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-102-3 Invalide wachtwoord', function (done) {
            chai.request(server)
                .post('/api/login')
                .send({
                    email: 'bcrypt4test.com',
                    password: 'secret'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-102-4 Gebruiker bestaat niet', function (done) {
            chai.request(server)
                .post('/api/login')
                .send({
                    email: 'ghost@test.com',
                    password: 'secr32131et'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.an('object');
                    done();
                });
        });
        it('TC-102-5 Gebruiker succesvol ingelogd', function (done) {
            chai.request(server)
                .post('/api/login')
                .send({
                    email: 'test2@test.com',
                    password: 'secret5'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.an('object');
                    const response = res.body;
                    response.should.have.property('token').which.is.a('string');
                    response.should.have
                        .property('username')
                        .which.is.a('string');
                    done();
                });
        });
    });
});
