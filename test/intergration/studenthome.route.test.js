process.env.DB_DATABASE = 'test_studenthome';
process.env.NODE_ENV = 'testing';
// console.log(`Running tests using database '${process.env.DB_DATABASE}'`);

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');
const pool = require('../../src/config/database');
const jwt = require('jsonwebtoken');

chai.should();
chai.use(chaiHttp);

const CLEAR_STUDENTHOME_TABLE = 'DELETE IGNORE FROM studenthome';
const CLEAR_USER_TABLE = 'DELETE IGNORE FROM user';

const INSERT_STUDENTHOMES = `INSERT INTO studenthome (Name, Address, House_Nr, UserID, Postal_Code, Telephone, City) VALUES 
('Princenhage', 'Princenhage', 11, ?,'4706RX','061234567891','Breda'),
('Haagdijk 23', 'Test', 4, ?, '4706RX','061234567891','Breda');`;

const INSERT_USER = `INSERT INTO user (First_Name, Last_Name, Email, Student_Number, Password) VALUES ('first', 'last', 'name@server.nl','1234567', 'secret');`;

let insertId;

before((done) => {
    pool.query(INSERT_USER, (err, rows, fields) => {
        if (err) {
            // console.log(`before INSERT_USER: ${err}`);
            done(err);
        } else {
            insertId = rows.insertId;
            done();
        }
    });
});

beforeEach((done) => {
    pool.query(CLEAR_STUDENTHOME_TABLE, (err, rows, fields) => {
        if (err) {
            // console.log(`beforeEach CLEAR_STUDENTHOME_TABLE: ${err}`);
            done(err);
        } else {
            done();
        }
    });
});

after((done) => {
    pool.query(CLEAR_STUDENTHOME_TABLE, (err, rows, fields) => {
        if (err) {
            // console.log(`after error: ${err}`);
            done(err);
        } else {
            pool.query(CLEAR_USER_TABLE, (err, rows, fields) => {
                if (err) {
                    // console.log(`beforeEach CLEAR_USER_TABLE: ${err}`);
                    done(err);
                } else {
                    done();
                }
            });
        }
    });
});

describe('Manage studenthome', () => {
    describe('UC-201 Create studenthome - POST /api/studenthome', () => {
        it('TC-201-1 should return valid error when required value is not present', (done) => {
            chai.request(server)
                .post('/api/studenthome')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                )
                .send({
                    // Name: 'Test',
                    Address: 'Test',
                    House_Nr: 1,
                    Postal_Code: '1234AB',
                    City: 'Test',
                    Telephone: '0612345678'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.an('object');

                    let { message, error } = res.body;
                    message.should.be
                        .an('string')
                        .that.equals('Error adding studenthome'); // and test for the correct value!
                    error.should.be.an('string').that.equals('name is missing'); // and test for the correct value!

                    done();
                });
        });

        it('TC-201-2 should return valid error when invalid postal code is posted', (done) => {
            chai.request(server)
                .post('/api/studenthome')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                )
                .send({
                    Name: 'Test',
                    Address: 'Test',
                    House_Nr: 1,
                    Postal_Code: '124A',
                    City: 'Test',
                    Telephone: '0612345678'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.an('object');

                    let { message, error } = res.body;
                    message.should.be
                        .an('string')
                        .that.equals('Error adding studenthome'); // and test for the correct value!
                    error.should.be
                        .an('string')
                        .that.equals('Incorrect postal code!'); // and test for the correct value!

                    done();
                });
        });

        it('TC-201-3 should return valid error when invalid telephone is posted', (done) => {
            chai.request(server)
                .post('/api/studenthome')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                )
                .send({
                    Name: 'Test',
                    Address: 'Test',
                    House_Nr: 1,
                    Postal_Code: '1234AB',
                    City: 'Test',
                    Telephone: '06123'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.should.be.an('object');

                    let { message, error } = res.body;
                    message.should.be
                        .an('string')
                        .that.equals('Error adding studenthome'); // and test for the correct value!
                    error.should.be
                        .an('string')
                        .that.equals('Incorrect telephone!'); // and test for the correct value!

                    done();
                });
        });

        it('TC-201-4 should return valid error when studenthome already exists', (done) => {
            pool.query(
                INSERT_STUDENTHOMES,
                [insertId, insertId],
                (err, result) => {
                    if (err) {
                        // console.log(`TC-201-4 INSERT_STUDENTHOMES: ${err}`);
                        done(err);
                    } else {
                        chai.request(server)
                            .post('/api/studenthome')
                            .set(
                                'authorization',
                                'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                            )
                            .send({
                                Name: 'Test',
                                Address: 'Test',
                                House_Nr: 1,
                                Postal_Code: '1234AB',
                                City: 'Test',
                                Telephone: '0612345678'
                            })
                            .end((err, res) => {
                                res.should.have.status(400);
                                res.should.be.an('object');

                                let { message, error } = res.body;
                                message.should.be
                                    .an('string')
                                    .that.equals(
                                        'the studenthome already exists!'
                                    ); // and test for the correct value!
                                error.should.be.an('string'); // and test for the correct value!

                                done();
                            });
                    }
                }
            );
        });

        it('TC-201-5 should return valid error when user is not logged in', (done) => {
            chai.request(server)
                .post('/api/studenthome')
                .send({
                    Name: 'Test',
                    Address: 'Test',
                    House_Nr: 1,
                    Postal_Code: '1234AB',
                    City: 'Test',
                    Telephone: '0612345678'
                })
                .end((err, res) => {
                    res.should.have.status(401);
                    res.should.be.an('object');

                    let { error } = res.body;
                    error.should.be
                        .an('string')
                        .that.equals('Authorization header missing!'); // and test for the correct value!

                    done();
                });
        });

        it('TC-201-6 should add studenthome to database when posting valid values', (done) => {
            chai.request(server)
                .post('/api/studenthome')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                )
                .send({
                    Name: 'Test',
                    Address: 'Test',
                    House_Nr: 1,
                    Postal_Code: '1234AB',
                    City: 'Test',
                    Telephone: '0612345678'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.should.be.an('object');

                    let { result } = res.body;

                    result.should.be
                        .an('object')
                        .that.has.property('id')
                        .that.is.an('number'); // and test for the correct value!

                    done();
                });
        });
    });

    describe('UC-202 Get all studenthomes / filter - GET /api/studenthome', () => {
        it('TC-202-1 should return 0 studenthomes', (done) => {
            chai.request(server)
                .get('/api/studenthome')
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');

                    let { error } = res.body;
                    error.should.be
                        .an('string')
                        .that.equals('the search doesnt exist!');
                    done();
                });
        });

        it('TC-202-2 should return 2 studenthomes', (done) => {
            pool.query(
                INSERT_STUDENTHOMES,
                [insertId, insertId],
                (err, result) => {
                    if (err) {
                        // console.log(`TC-201-4 INSERT_STUDENTHOMES: ${err}`);
                        done(err);
                    } else {
                        chai.request(server)
                            .get('/api/studenthome')
                            .end((err, res) => {
                                res.should.have.status(200);
                                res.should.be.an('object');

                                let { result } = res.body;
                                result.should.be.an('array').that.has.length(2);

                                done();
                            });
                    }
                }
            );
        });

        it('TC-202-3 should return 404 when the city has no student houses', (done) => {
            chai.request(server)
                .get('/api/studenthome/?City=45465')
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');

                    let { error } = res.body;
                    error.should.be
                        .an('string')
                        .that.equals('the search doesnt exist!');

                    done();
                });
        });

        it('TC-202-4 should return 404 when the name has no student houses', (done) => {
            chai.request(server)
                .get('/api/studenthome/?Name=45465')
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');

                    let { error } = res.body;
                    error.should.be
                        .an('string')
                        .that.equals('the search doesnt exist!');

                    done();
                });
        });
    });

    describe('UC-203 Get Student home details - get /api/studenthome/:studenthomeID', () => {
        it('TC-203-1 should return 404 when house is not there', (done) => {
            chai.request(server)
                .get('/api/studenthome/500000')

                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');

                    let { error } = res.body;
                    error.should.be
                        .an('string')
                        .that.equals('the search doesnt exist!');

                    done();
                });
        });

        it('TC-203-2 should return 200 when house is there', (done) => {
            chai.request(server)
                .get('/api/studenthome/1')

                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');

                    let { message } = res.body;

                    done();
                });
        });
    });

    describe('UC-205 Studentenhuis verwijderen', () => {
        it('TC-205-1 Studentenhuis bestaat niet', (done) => {
            chai.request(server)
                .delete('/api/studenthome/500000')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                )
                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');

                    done();
                });
        });

        it('TC-205-2 Niet ingelogd', (done) => {
            chai.request(server)
                .delete('/api/studenthome/1')

                .end((err, res) => {
                    res.should.have.status(401);
                    res.should.be.an('object');

                    let { error } = res.body;
                    error.should.be
                        .an('string')
                        .that.equals('Authorization header missing!');

                    done();
                });
        });

        it('TC-205-3 Actor is geen eigenaar', (done) => {
            chai.request(server)
                .delete('/api/studenthome/1')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: 43244242 }, 'secret')
                )

                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');

                    done();
                });
        });
        it('TC-205-4 Studentenhuis  succesvol verwijderd', (done) => {
            chai.request(server)
                .delete('/api/studenthome/1')
                .set(
                    'authorization',
                    'Bearer ' + jwt.sign({ id: insertId }, 'secret')
                )

                .end((err, res) => {
                    res.should.have.status(404);
                    res.should.be.an('object');

                    done();
                });
        });
    });
});
