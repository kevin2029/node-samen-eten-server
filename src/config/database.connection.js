const pool = require('../config/database');
const config = require('../config/config');
const logger = config.logger;

let connection = {
    connectDatabase(query, parameters, callback) {
        pool.getConnection((err, connection) => {
            if (err) {
                res.status(400).json({
                    message: 'database connection failed!',
                    error: err
                });
            }
            connection.query(query, parameters, (error, results, fields) => {
                connection.release();
                logger.debug('releasing connection');
                callback(error, results);
            });
        });
    }
};

module.exports = connection;
