const express = require('express');
const router = express.Router();
const AuthController = require('../controllers/authenication.controller');

//UC-101
router.post(
    '/register',
    AuthController.validateRegister,
    AuthController.register
);

//UC-102
router.post('/login', AuthController.validateLogin, AuthController.login);

module.exports = router;
