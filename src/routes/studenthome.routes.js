const express = require('express');
const router = express.Router();
const studenthomeController = require('../controllers/studenthome.controller');
const authController = require('../controllers/authenication.controller');

//UC-201
router.post(
    '/studenthome',
    authController.validateToken,
    studenthomeController.validateHouse,
    studenthomeController.createHouse
);

//UC-202
router.get('/studenthome', studenthomeController.getAll);

// //UC-203
router.get('/studenthome/:homeId', studenthomeController.getById);

//UC-204
router.put(
    '/studenthome/:homeId',
    authController.validateToken,
    studenthomeController.validateHouse,
    studenthomeController.changeHouseById
);

//UC-205
router.delete(
    '/studenthome/:homeId',
    authController.validateToken,
    studenthomeController.deleteById
);

//UC-206??

module.exports = router;
