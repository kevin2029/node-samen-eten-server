const express = require('express');
const router = express.Router();
const mealController = require('../controllers/meal.controller');
const authController = require('../controllers/authenication.controller');

//UC-301
router.post(
    '/studenthome/:homeId/meal',
    authController.validateToken,
    mealController.validateMeal,
    mealController.createMeal
);

//UC-302
router.put(
    '/studenthome/:homeId/meal/:mealId',
    authController.validateToken,
    mealController.validateMeal,
    mealController.changeMealById
);
//UC-303
router.get('/studenthome/:homeId/meal', mealController.getAll);

//UC-304
router.get('/studenthome/:homeId/meal/:mealId', mealController.getById);

//UC-305
router.delete(
    '/studenthome/:homeId/meal/:mealId',
    authController.validateToken,
    mealController.deleteById
);
module.exports = router;
