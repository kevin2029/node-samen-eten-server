const express = require('express');
const router = express.Router();
const participantController = require('../controllers/participant.controller');
const authController = require('../controllers/authenication.controller');

//UC-401
router.post(
    '/studenthome/:homeId/meal/:mealId/signup',
    authController.validateToken,
    participantController.validateParticipant,
    participantController.checkMaxParticipants,
    participantController.signup
);

//UC-402
router.delete(
    '/studenthome/:homeId/meal/:mealId/signoff',
    authController.validateToken,
    participantController.signoff
);

//UC-403
router.get(
    '/meal/:mealId/participants',
    authController.validateToken,
    participantController.getAllParticipant
);

//Uc-404
router.get(
    '/meal/:mealId/participants/:participantId',
    authController.validateToken,
    participantController.getParticipantById
);
module.exports = router;
