const config = require('../config/config');
const logger = config.logger;
const assert = require('assert');
const mealRepository = require('../repository/meal.repository');
const connection = require('../config/database.connection');

let controller = {
    validateMeal(req, res, next) {
        const {
            Name,
            Description,
            Ingredients,
            Allergies,
            OfferedOn,
            Price,
            MaxParticipants
        } = req.body;
        //validate that missing value gives error
        try {
            assert(typeof Name === 'string', 'name is missing');
            assert(typeof Description === 'string', 'description is missing');
            assert(typeof Ingredients === 'string', 'ingredients is missing');
            assert(typeof Allergies === 'string', 'allergies is missing');
            assert(typeof OfferedOn === 'string', 'offeredOn is missing');
            assert.match(
                OfferedOn,
                /^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/,
                'Invalid OfferedOn date!'
            );
            assert(typeof Price === 'string', 'price is missing');

            assert.match(Price, /(\d+\.\d{1,2})/, 'Invalid Price!');

            assert(
                typeof MaxParticipants === 'number',
                'maxParticipants is missing'
            );
            next();
        } catch (err) {
            res.status(400).json({
                message: 'Error adding meal',
                error: err.toString()
            });
        }
    },

    createMeal(req, res, next) {
        logger.info('Post called on /api/studenthome/:homeId/meal');
        const meal = req.body;
        const homeId = req.params.homeId;
        const userId = req.userId;
        logger.debug('meal: ', meal);

        let {
            Name,
            Description,
            Ingredients,
            Allergies,
            CreatedOn,
            OfferedOn,
            Price,
            MaxParticipants
        } = meal;

        let sqlQuery =
            'INSERT INTO `meal` (`Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ';

        logger.debug('createStudenthome', 'sqlQuery =', sqlQuery);

        connection.connectDatabase(
            sqlQuery,
            [
                Name,
                Description,
                Ingredients,
                Allergies,
                CreatedOn,
                OfferedOn,
                Price,
                userId,
                homeId,
                MaxParticipants
            ],
            (error, results) => {
                if (error) {
                    logger.error('createMeal', error);
                    res.status(400).json({
                        message: 'createMeal failed',
                        error: error
                    });
                }
                if (results) {
                    logger.trace('results: ', results);
                    res.status(200).json({
                        result: {
                            id: results.insertId,
                            ...meal
                        }
                    });
                }
            }
        );
    },

    changeMealById(req, res, next) {
        const mealId = req.params.mealId;
        const homeId = req.params.homeId;
        const meal = req.body;
        const userid = req.userId;
        logger.info('put request on /api/studenthome/:homeId/meal/:mealId');
        logger.debug('meal: ', meal);

        let {
            Name,
            Description,
            Ingredients,
            Allergies,
            CreatedOn,
            OfferedOn,
            Price,
            MaxParticipants
        } = meal;

        let sqlQuery =
            'UPDATE `meal` SET ' +
            'Name = ?, Description = ?, Ingredients = ?, Allergies = ?, CreatedOn = ?, OfferedOn = ?, Price = ?, MaxParticipants = ? WHERE ID = ' +
            mealId +
            ' ' +
            'AND StudenthomeID = ' +
            homeId +
            ' ' +
            'AND UserID = ' +
            userid;
        logger.debug('createStudenthome', 'sqlQuery =', sqlQuery);

        connection.connectDatabase(
            sqlQuery,
            [
                Name,
                Description,
                Ingredients,
                Allergies,
                CreatedOn,
                OfferedOn,
                Price,
                MaxParticipants
            ],
            (error, results) => {
                if (error) {
                    logger.error('createMovie', error);
                    res.status(400).json({
                        message: 'change studenthome failed calling query',
                        error: error
                    });
                }
                if (results) {
                    if (results.affectedRows === 0) {
                        logger.trace('item was NOT edited');
                        res.status(401).json({
                            result: {
                                error:
                                    'Item not found of you do not have access to this item'
                            }
                        });
                    } else {
                        logger.trace('results: ', results);
                        res.status(200).json({
                            changedResult: meal
                        });
                    }
                }
            }
        );
    },

    getAll(req, res, next) {
        logger.info('Get request on /api/studenthome/meal');

        const homeId = req.params.homeId;

        mealRepository.getId(homeId, (error, results) => {
            if (error) {
                res.status(400).json({
                    message: 'GetAll failed!',
                    error: error
                });
            }
            if (results) {
                logger.trace('results: ', results);

                if (results.length === 0) {
                    res.status(400).json({
                        error: 'the search doesnt exist!'
                    });
                } else {
                    res.status(200).json({
                        result: results
                    });
                }
            }
        });
    },

    getById(req, res, next) {
        logger.info('Get request on /api/studenthome/:homeId/meal/:mealId');
        const mealId = req.params.mealId;
        const homeId = req.params.homeId;
        logger.debug('index of meal: ', mealId, 'homeID: ', homeId);

        let sqlQuery =
            'SELECT * FROM meal' +
            (mealId ? ' WHERE ID = ?' : '') +
            (homeId ? ' AND StudenthomeID = ?' : '');
        logger.debug('getById', 'sqlQuery =', sqlQuery);

        connection.connectDatabase(
            sqlQuery,
            [mealId, homeId],
            (error, results) => {
                if (error) {
                    res.status(400).json({
                        message: 'GetById failed!',
                        error: error
                    });
                }
                if (results) {
                    logger.trace('results: ', results);

                    if (results.length === 0) {
                        res.status(400).json({
                            error: 'the search doesnt exist!'
                        });
                    } else {
                        res.status(200).json({
                            result: results
                        });
                    }
                }
            }
        );
    },
    deleteById(req, res, next) {
        const homeId = req.params.homeId;
        const mealId = req.params.mealId;
        const userId = req.userId;
        logger.info(
            'delete request on /studenthome/:homeId/meal/:mealId',
            'id = ',
            mealId,
            'homeid = ',
            homeId,
            'userid = ',
            userId
        );

        let sqlQuery =
            'DELETE FROM meal WHERE id = ? AND studenthomeID = ? AND UserID = ?';

        connection.connectDatabase(
            sqlQuery,
            [mealId, homeId, userId],
            (error, results) => {
                if (error) {
                    res.status(400).json({
                        message: 'Could not delete item',
                        error: error
                    });
                }
                if (results) {
                    if (results.affectedRows === 0) {
                        logger.trace('item was NOT deleted');
                        res.status(401).json({
                            result: {
                                error:
                                    'Item not found of you do not have access to this item'
                            }
                        });
                    } else {
                        logger.trace('item was deleted');
                        res.status(200).json({
                            result: 'item was deleted'
                        });
                    }
                }
            }
        );
    }
};

module.exports = controller;
