const config = require('../config/config');
const logger = config.logger;
const assert = require('assert');
const connection = require('../config/database.connection');

let controller = {
    validateParticipant(req, res, next) {
        const { UserId } = req.body;
        //validate that missing value gives error
        try {
            assert(typeof UserId === 'number', 'UserId is missing');

            next();
        } catch (err) {
            res.status(400).json({
                message: 'Error validating',
                error: err.toString()
            });
        }
    },
    checkMaxParticipants(req, res, next) {
        const mealId = req.params.mealId;

        let sqlQuery = 'SELECT MaxParticipants FROM meal WHERE ID = ' + mealId;
        connection.connectDatabase(sqlQuery, (error, results) => {
            if (error) {
                res.status(400).json({
                    message: 'Meal does not exist!'
                });
            }
            if (results) {
                const maxParticipants = results[0].MaxParticipants;

                sqlQuery =
                    'SELECT * FROM participants WHERE MealID = ' + mealId;
                connection.connectDatabase(sqlQuery, (err, results) => {
                    if (err) {
                        res.status(400).json({
                            error: err
                        });
                    } else {
                        if (results.length >= maxParticipants) {
                            res.status(400).json({
                                message:
                                    'No more room for participants for this meal!'
                            });
                        } else {
                            next();
                        }
                    }
                });
            }
        });
    },
    signup(req, res, next) {
        logger.info(
            'Post called on /api/studenthome/:homeId/meal/:mealId/signup'
        );

        const userid = req.userId;
        const homeId = req.params.homeId;
        const mealId = req.params.mealId;
        const signedUpOn = new Date().toISOString();

        logger.debug(userid, req.body.UserId);

        //Check if the id input == the token
        if (userid !== req.body.UserId) {
            logger.debug('signup error');
            res.status(400).json({
                message: 'failed sign up! Please enter the correnct userid'
            });
        } else {
            let sqlQuery =
                'INSERT INTO `participants` (`UserID`, `StudenthomeID`, `MealID`, `SignedUpOn`) VALUES (?, ?, ?, ?) ';
            logger.debug('signup', 'sqlQuery =', sqlQuery);

            connection.connectDatabase(
                sqlQuery,
                [userid, homeId, mealId, signedUpOn],
                (error, results) => {
                    if (error) {
                        logger.error('signup', error);
                        res.status(400).json({
                            message:
                                'failed adding! you are already signed in for this meal! or the meal does not exists!'
                        });
                    }
                    if (results) {
                        if (results.affectedRows === 0) {
                            res.status(400).json({
                                error: 'the input data was invalid'
                            });
                        } else {
                            logger.trace('results: ', results);
                            res.status(200).json({
                                result: 'succes you are added!'
                            });
                        }
                    }
                }
            );
        }
    },

    signoff(req, res, next) {
        logger.info(
            'Post called on /api/studenthome/:homeId/meal/:mealId/signoff'
        );

        const userid = req.userId;
        const homeId = req.params.homeId;
        const mealId = req.params.mealId;

        logger.debug(userid, req.body.UserId);

        //check if the id input == token
        if (userid !== req.body.UserId) {
            logger.debug('signup error');
            res.status(400).json({
                message: 'failed delete! Please enter the correnct userid'
            });
        } else {
            let sqlQuery =
                'DELETE FROM participants WHERE UserID = ? AND StudenthomeID = ? AND MealID = ?';
            logger.debug('signup', 'sqlQuery =', sqlQuery);

            connection.connectDatabase(
                sqlQuery,
                [userid, homeId, mealId],
                (error, results) => {
                    if (error) {
                        logger.error('signup', error);
                        res.status(400).json({
                            message:
                                'failed delete! you are already signed off in for this meal! or the meal doesnt exists!'
                        });
                    }
                    if (results) {
                        logger.trace('results: ', results);

                        if (results.affectedRows === 0) {
                            res.status(400).json({
                                error:
                                    'Meal not found or you do not have access to this item'
                            });
                        } else {
                            res.status(200).json({
                                result: 'succes you are signed off!'
                            });
                        }
                    }
                }
            );
        }
    },

    getAllParticipant(req, res, next) {
        logger.info('Post called on /api/meal/:mealId/participants');
        const mealId = req.params.mealId;
        const userId = req.userId;

        let sqlQuery =
            'SELECT participants.UserId,user.First_Name, user.Last_Name,user.Email, meal.Name as meal, OfferedOn ' +
            'FROM participants ' +
            'JOIN meal ON participants.MealID = meal.ID JOIN user ON participants.UserId = user.ID ' +
            'WHERE meal.UserID = ? AND mealID = ? ';

        logger.debug('getAllParticipant', 'sqlQuery =', sqlQuery);

        connection.connectDatabase(
            sqlQuery,
            [userId, mealId],
            (error, results) => {
                if (error) {
                    logger.error('getAllParticipant', error);
                    res.status(400).json({
                        message: 'getAllParticipant failed calling query',
                        error: error
                    });
                }
                if (results) {
                    logger.trace('results: ', results);

                    if (results.length === 0) {
                        res.status(400).json({
                            error:
                                'Item not found or you do not have access to this item'
                        });
                    } else {
                        res.status(200).json({
                            result: results
                        });
                    }
                }
            }
        );
    },

    getParticipantById(req, res, next) {
        logger.info(
            'Post called on /api/meal/:mealId/participants/:participantId'
        );

        const mealId = req.params.mealId;
        const participantId = req.params.participantId;
        const userId = req.userId;

        let sqlQuery =
            'SELECT participants.UserId,user.First_Name, user.Last_Name,user.Email, meal.Name as meal, OfferedOn ' +
            'FROM participants ' +
            'JOIN meal ON participants.MealID = meal.ID JOIN user ON participants.UserId = user.ID ' +
            'WHERE meal.UserID = ? AND mealID = ? AND participants.UserID = ?';

        logger.debug('getParticipantById', 'sqlQuery =', sqlQuery);

        connection.connectDatabase(
            sqlQuery,
            [userId, mealId, participantId],
            (error, results) => {
                if (results) {
                    if (results.length === 0) {
                        res.status(400).json({
                            error:
                                'Item not found or you do not have access to this item'
                        });
                    } else {
                        res.status(200).json({
                            result: results
                        });
                    }
                }
                if (error) {
                    res.status(401).json({
                        message: 'getParticipantById failed no results',
                        error: error
                    });
                }
            }
        );
    }
};

module.exports = controller;
