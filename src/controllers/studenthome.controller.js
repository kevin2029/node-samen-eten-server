const config = require('../config/config');
const logger = config.logger;
const assert = require('assert');
const mealRepository = require('../repository/meal.repository');
const connection = require('../config/database.connection');

let controller = {
    validateHouse(req, res, next) {
        const {
            Name,
            Address,
            House_Nr,
            Postal_Code,
            Telephone,
            City
        } = req.body;

        //validate that missing value gives error
        try {
            assert(typeof Name === 'string', 'name is missing');
            assert(typeof Address === 'string', 'street is missing');
            assert(typeof House_Nr === 'number', 'houseNumber is missing');
            assert(typeof Postal_Code === 'string', 'postalCode is missing');
            assert(typeof Telephone === 'string', 'number is missing');
            assert(typeof City === 'string', 'city is missing');

            //Check if the input matches the RegEx
            assert.match(
                Telephone,
                /^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)[1-9]((\s|\s?\-\s?)?[0-9])((\s|\s?-\s?)?[0-9])((\s|\s?-\s?)?[0-9])\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]$/,
                'Incorrect telephone!'
            );
            assert.match(
                Postal_Code,
                /^\d{4}\s?\w{2}$/,
                'Incorrect postal code!'
            );

            next();
        } catch (err) {
            res.status(400).json({
                message: 'Error adding studenthome',
                error: err.message
            });
        }
    },

    createHouse(req, res, next) {
        logger.info('Post called on /api/studenthome');
        const house = req.body;
        logger.debug('house: ', house);

        let { Name, Address, House_Nr, Postal_Code, Telephone, City } = house;
        const userid = req.userId;
        logger.debug('user id: ', userid);
        let sqlQuery =
            'INSERT INTO `studenthome` (`Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES (?, ?, ?, ?, ?, ?, ?)';
        logger.debug('createStudenthome', 'sqlQuery =', sqlQuery);

        connection.connectDatabase(
            sqlQuery,
            [Name, Address, House_Nr, userid, Postal_Code, Telephone, City],
            (error, results) => {
                if (error) {
                    logger.debug('createStudenthome', error);
                    res.status(400).json({
                        message: 'the studenthome already exists!',
                        error: error.message
                    });
                }
                if (results) {
                    logger.trace('results: ', results);
                    res.status(200).json({
                        result: {
                            id: results.insertId,
                            userId: userid,
                            ...house
                        }
                    });
                }
            }
        );
    },
    getAll(req, res, next) {
        logger.info('Get request on /api/studenthome');
        const seachCity = req.query.City;
        const seachPart = req.query.Name;

        let sqlQuery = '';

        //Check what filters are going to be used
        if (seachPart != null && seachCity != null) {
            sqlQuery =
                'SELECT * FROM studenthome' +
                (seachCity ? ' WHERE City = ?' : '') +
                " AND Name LIKE '" +
                seachPart +
                "%'";
        } else if (seachCity != null) {
            sqlQuery =
                'SELECT * FROM studenthome' +
                (seachCity ? ' WHERE City = ?' : '');
        } else if (seachPart != null) {
            sqlQuery =
                "SELECT * FROM studenthome WHERE Name LIKE '" +
                seachPart +
                "%'";
        }

        //if no filters are given
        if (seachPart === undefined && seachCity === undefined) {
            sqlQuery = ' SELECT * FROM studenthome;';
        }

        logger.debug('getAll', 'sqlQuery =', sqlQuery);

        connection.connectDatabase(sqlQuery, seachCity, (error, results) => {
            if (error) {
                res.status(400).json({
                    message: 'GetAll failed!',
                    error: error
                });
            }
            if (results) {
                logger.trace('results: ', results);

                if (results.length === 0) {
                    res.status(404).json({
                        error: 'the search doesnt exist!'
                    });
                } else {
                    res.status(200).json({
                        result: results
                    });
                }
            }
        });
    },

    getById(req, res, next) {
        logger.info('Get request on /api/studenthome/:homeId');
        const homeId = req.params.homeId;
        logger.debug('index of studenthome: ', homeId);

        let sqlQuery =
            'SELECT * FROM studenthome ' +
            (homeId ? ' WHERE studenthome.ID = ?' : '');
        logger.debug('getById', 'sqlQuery =', sqlQuery);

        connection.connectDatabase(sqlQuery, [homeId], (error, results) => {
            mealRepository.getId(homeId, (error2, results2) => {
                // Handle error after the release.
                if (error || error2) {
                    res.status(404).json({
                        message: 'GetById failed!',
                        error: error || error2
                    });
                }
                if (results) {
                    logger.trace('results: ', results);

                    if (results.length === 0) {
                        res.status(404).json({
                            error: 'the search doesnt exist!'
                        });
                    } else if (results2.length === 0) {
                        res.status(200).json({
                            result: results,
                            meals: 'no meals for this studenthome'
                        });
                    } else {
                        res.status(200).json({
                            result: results,
                            meals: results2
                        });
                    }
                }
            });
        });
    },

    changeHouseById(req, res, next) {
        const id = req.params.homeId;
        const house = req.body;
        const userid = req.userId;
        logger.info('put request on /api/studenthome/:homeId');
        logger.debug('house: ', house);

        let { Name, Address, House_Nr, Postal_Code, Telephone, City } = house;

        let sqlQuery =
            'UPDATE `studenthome` SET ' +
            'Name = ?, Address = ?, House_Nr = ?, UserID = ?, Postal_Code = ?, Telephone = ?, City = ? WHERE ID = ' +
            id +
            ' AND userid = ' +
            userid;
        logger.debug('createStudenthome', 'sqlQuery =', sqlQuery);

        connection.connectDatabase(
            sqlQuery,
            [Name, Address, House_Nr, userid, Postal_Code, Telephone, City],
            (error, results) => {
                if (error) {
                    logger.debug('createMovie', error);
                    res.status(400).json({
                        message: 'change studenthome failed calling query',
                        error: error
                    });
                }
                if (results) {
                    if (results.affectedRows === 0) {
                        logger.trace('item was NOT edited');
                        res.status(401).json({
                            result: {
                                error:
                                    'Studenthome not found or you do not have access to this item'
                            }
                        });
                    } else {
                        logger.trace('results: ', results);
                        res.status(200).json({
                            changedResult: house
                        });
                    }
                }
            }
        );
    },
    deleteById(req, res, next) {
        const id = req.params.homeId;
        const userid = req.userId;
        logger.info(
            'delete request on /api/studenthome/:homeId',
            'id =',
            id,
            'userid =',
            userid
        );

        let sqlQuery = 'DELETE FROM studenthome WHERE id = ? AND UserID = ?';

        connection.connectDatabase(sqlQuery, [id, userid], (error, results) => {
            if (error) {
                res.status(400).json({
                    message: 'Delete studenthome failed calling query',
                    error: error
                });
            }
            if (results) {
                if (results.affectedRows === 0) {
                    logger.trace('item was NOT deleted');
                    res.status(404).json({
                        result: {
                            error:
                                'Studenthome not found or you do not have access to this item'
                        }
                    });
                } else {
                    logger.trace('item was deleted');
                    res.status(200).json({
                        result: 'item was deleted'
                    });
                }
            }
        });
    }
};

module.exports = controller;
