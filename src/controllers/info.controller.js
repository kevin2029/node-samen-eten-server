const config = require('../config/config').logger;

let constroller = {
    info(req, res) {
        const info = {
            Name: 'Kevin Nguyen',
            StudentNumber: '2150956',
            Description: 'A server for school',
            sonarqubeURL:
                'https://sonarqube.avans-informatica-breda.nl/dashboard?id=node-prog4'
        };

        res.status(200).json(info);
    }
};

module.exports = constroller;
