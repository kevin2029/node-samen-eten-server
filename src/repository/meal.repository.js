const pool = require('../config/database');
const config = require('../config/config');
const logger = config.logger;

let repository = {
    getId(homeid, callback) {
        let sqlQuery =
            'SELECT * FROM meal' + (homeid ? ' WHERE StudenthomeID = ?' : '');

        logger.debug('getAll', 'sqlQuery =', sqlQuery);

        pool.getConnection(function (err, connection) {
            if (err) {
                res.status(400).json({
                    message: 'GetAll failed!',
                    error: err
                });
            }

            connection.query(sqlQuery, [homeid], (error, results, fields) => {
                connection.release();

                callback(error, results);
            });
        });
    }
};

module.exports = repository;
